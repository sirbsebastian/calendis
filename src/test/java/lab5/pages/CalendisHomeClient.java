package lab5.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.util.ArrayList;
import java.util.List;

@DefaultUrl("https://testclient.calendis.ro/")
public class CalendisHomeClient extends PageObject {


    @FindBy(id = "submit-recommend-business")
    private WebElementFacade submitRecomanda;

    @FindBy(id = "login-btn")
    private WebElementFacade loginButton;


    @FindBy(id = "forEmail")
    private WebElementFacade loginEmailClient;


    @FindBy(id = "forPassword")
    private WebElementFacade loginPasswordClient;


    @FindBy(id = "submit-login")
    private WebElementFacade submitLogin;

    @FindBy(id = "service-search")
    private WebElementFacade searchElement;

    @FindBy(className = "select-option")
    public WebElementFacade chooseOption;


    @FindBy(id = "search-btn")
    private WebElementFacade searchButton;

    @FindBy(className = "date-container")
    private WebElementFacade searchResult;

    @FindBy(className = "highlight-link underline-link")
    private WebElementFacade linkRecomanda;

    @FindBy(id = "business-name")
    private WebElementFacade inputNume;

    @FindBy(id = "business-location")
    private WebElementFacade locationInput;

    @FindBy(id = "auth-lastname")
    private WebElementFacade lastNameInput;

    @FindBy(id = "auth-firstname")
    private WebElementFacade firstNameInput;

    @FindBy(id = "auth-email")
    private WebElementFacade emailnput;

    @FindBy(id = "auth-phone-number")
    private WebElementFacade phoneInput;


    public void enterEmailClient(String email) {
        loginEmailClient.type(email);
    }

    public void inputNume(String nume) {
        inputNume.type(nume);
    }

    public void inputLocation(String location) {
        locationInput.type(location);
    }


    public void inputLastName(String location) {
        lastNameInput.type(location);
    }

    public void inputFirstName(String location) {
        firstNameInput.type(location);
    }

    public void inputEmail(String location) {
        emailnput.type(location);
    }

    public void inputPhone(String location) {
        phoneInput.type(location);
    }


    public void clickLogIn() {
        loginButton.click();
    }


    public void enterPasswordClient(String password) {
        loginPasswordClient.type(password);
    }

    public void submitLogin() {
        submitLogin.click();
    }

    public void submitLoginClient() {
        loginPasswordClient.sendKeys(Keys.ENTER);

    }

    public void enterSearchElement(String cautat) {
        searchElement.type(cautat);
    }

    public void chooseOption() {
        chooseOption.click();
    }

    public void clickSearch() {
        searchButton.click();
    }

    public String serachResultIs() {
        return searchResult.getText();
    }

    public void clickLinkRecom() {
        // linkRecomanda.cssSe();
    }

    public void submitRecomanda() {
        submitRecomanda.click();
    }
}