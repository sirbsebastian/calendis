package lab5.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://testbusiness.calendis.ro/")
public class CalendisHomeAdmin {
    @FindBy(id = "login-btn")
    private WebElementFacade loginButton;
    @FindBy(id = "login-email")
    private WebElementFacade loginEmail;
    @FindBy(id = "login-password")
    private WebElementFacade loginPassword;

    public void clickLogIn() {
        loginButton.click();
    }

    public void clickLogInClient() {
        loginButton.click();
    }

    public void enterEmail(String email) {
        loginEmail.type(email);
    }

    public void enterPassword(String password) {
        loginPassword.type(password);
    }
}
