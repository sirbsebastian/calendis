package lab5.feature;

import lab5.steps.serenity.LoginStep;
import lab5.steps.serenity.RecomandaStep;
import lab5.steps.serenity.SearchStep;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.*;

import java.util.ArrayList;

import static lab5.Utils.Crypt.decrypt;
import static lab5.Utils.Crypt.getRandomNumberInRange;

@RunWith(SerenityRunner.class)
public class CalendisStory {
    @Managed(uniqueSession = true)
    public WebDriver webDriver;

    @Steps
    public LoginStep calendisLogIn;

    @Steps
    public SearchStep calendisSerach;
    @Steps
    public RecomandaStep calendisRecom;


    @Test
    public void loginTest() {
        webDriver.manage().window().fullscreen();

        calendisLogIn.open();
        calendisLogIn.clickOnSignInClient();
        calendisLogIn.enterEmailClient("sirbsebastian97@gmail.com");
        calendisLogIn.enterPassClient(decrypt("2GWHmgZQNt/WVI/XUwTw3Q=="));
        calendisLogIn.submitLogin();
    }

    @Test
    public void testSearch() throws InterruptedException {
        webDriver.manage().window().fullscreen();

        calendisLogIn.open();
        calendisLogIn.clickOnSignInClient();
        calendisLogIn.enterEmailClient("sirbsebastian97@gmail.com");
        calendisLogIn.enterPassClient(decrypt("2GWHmgZQNt/WVI/XUwTw3Q=="));
        calendisLogIn.submitLogin();

        calendisSerach.search("Asigurari_test");
        calendisSerach.submitSerach();

        if (calendisSerach.getSearchResult().equals("Rezultate găsite: 1")) {
            assert true;
        } else assert false;


    }

    @Test
    public void testRecomanda() {
        webDriver.manage().window().fullscreen();

        calendisLogIn.open();
        calendisLogIn.clickOnSignInClient();
        calendisLogIn.enterEmailClient("sirbsebastian97@gmail.com");
        calendisLogIn.enterPassClient(decrypt("2GWHmgZQNt/WVI/XUwTw3Q=="));
        calendisLogIn.submitLogin();

        JavascriptExecutor jse = (JavascriptExecutor) webDriver;
        jse.executeScript("scroll(0, 750)");
        webDriver.findElement(By.partialLinkText("aici")).click();
        calendisRecom.writeNume("Emergent");
        calendisRecom.writeLocation("Aiud");
        calendisRecom.submitRecomanda();

        String text = webDriver.findElement(By.className("response-text-container")).getText();
        String firstLine = text.substring(0, text.indexOf("\n"));
        if (firstLine.equals("Succes!")) {
            assert true;
        } else assert false;

    }


    @Test
    public void testRecomandaFull() {
        webDriver.manage().window().fullscreen();

        calendisLogIn.open();
        calendisLogIn.clickOnSignInClient();
        calendisLogIn.enterEmailClient("sirbsebastian97@gmail.com");
        calendisLogIn.enterPassClient(decrypt("2GWHmgZQNt/WVI/XUwTw3Q=="));
        calendisLogIn.submitLogin();

        JavascriptExecutor jse = (JavascriptExecutor) webDriver;
        jse.executeScript("scroll(0, 750)");
        webDriver.findElement(By.partialLinkText("aici")).click();
        calendisRecom.writeNume("Glovo");
        calendisRecom.writeLocation("Alba");
        calendisRecom.writeLastName("Sirb");
        calendisRecom.writeFirstName("Sebastian");
        calendisRecom.writeEmail("sebi@yahoo.com");
        calendisRecom.writePhone("0749785477");
        calendisRecom.submitRecomanda();

        String text = webDriver.findElement(By.className("response-text-container")).getText();
        String firstLine = text.substring(0, text.indexOf("\n"));
        if (firstLine.equals("Succes!")) {
            assert true;
        } else assert false;

    }

    @Test
    public void testRecomandaFullNonLogin() {
        webDriver.manage().window().fullscreen();

        calendisLogIn.open();
        JavascriptExecutor jse = (JavascriptExecutor) webDriver;
        jse.executeScript("scroll(0, 750)");
        webDriver.findElement(By.partialLinkText("aici")).click();
        calendisRecom.writeNume("Magnolia");
        calendisRecom.writeLocation("Teius");
        calendisRecom.writeLastName("Sirb");
        calendisRecom.writeFirstName("George");
        calendisRecom.writeEmail("sebi@yahoo.com");
        calendisRecom.writePhone("0749785477");
        calendisRecom.submitRecomanda();

        String text = webDriver.findElement(By.className("response-text-container")).getText();
        String firstLine = text.substring(0, text.indexOf("\n"));
        if (firstLine.equals("Succes!")) {
            assert true;
        } else assert false;

    }

   // @Test
    public void testRecomandaInvalidPhone() {
        webDriver.manage().window().fullscreen();

        calendisLogIn.open();
        calendisLogIn.clickOnSignInClient();
        calendisLogIn.enterEmailClient("sirbsebastian97@gmail.com");
        calendisLogIn.enterPassClient(decrypt("2GWHmgZQNt/WVI/XUwTw3Q=="));
        calendisLogIn.submitLogin();

        JavascriptExecutor jse = (JavascriptExecutor) webDriver;
        jse.executeScript("scroll(0, 750)");
        webDriver.findElement(By.partialLinkText("aici")).click();
        calendisRecom.writeNume("Spartan");
        calendisRecom.writeLocation("Iernut");
        calendisRecom.writeLastName("Man");
        calendisRecom.writeFirstName("Andrei");
        calendisRecom.writeEmail("sebi@yahoo.com");
        calendisRecom.writePhone("0749785488");
        calendisRecom.submitRecomanda();

        String text = webDriver.findElement(By.id("phone-number-error")).getText();
        System.out.println(text);
        if (text.equals("Număr de telefon în format invalid.")) {
            assert true;
        } else assert false;

    }

    @Test
    public void programareTest() {
        webDriver.manage().window().fullscreen();
        int random = getRandomNumberInRange(1, 8);

        calendisLogIn.open();
        calendisLogIn.clickOnSignInClient();
        calendisLogIn.enterEmailClient("sirbsebastian97@gmail.com");
        calendisLogIn.enterPassClient(decrypt("2GWHmgZQNt/WVI/XUwTw3Q=="));
        calendisLogIn.submitLogin();
        calendisSerach.search("Asigurari_test");
        calendisSerach.submitSerach();
        webDriver.findElement(By.partialLinkText("VEZI MAI MULTE")).click();
        WebElement elem = webDriver.findElement(By.className("col-xs-6"));
        elem.click();
        webDriver.findElement(By.xpath("//div[@id='appointment-calendar']/div[@class='calendis-days']/div[@class='calendar-arrow right-arrow']/div[@class='arrow-img-holder']")).click();
        System.out.println(webDriver.findElement(By.id("appointment-slots")));
        ArrayList<WebElement> lista= (ArrayList<WebElement>) webDriver.findElements(By.className("slot-time"));
        for (WebElement element:lista
             ) {
            System.out.println(element);

        }
        webDriver.findElement(By.xpath("//div[@id='appointment-slots']/div["+random+"]/div[@class='slot-item']\n")).click();
        webDriver.findElement(By.xpath("/html//button[@id='submit-appointment']\n")).click();
        webDriver.findElement(By.xpath("/html//button[@id='confirm-appointment']\n")).click();
        String text = webDriver.findElement(By.className("success-response")).getText();
        System.out.println(text);
        String substring = text.substring(0, text.indexOf("\n"));
        System.out.println(substring);
        if (substring.equals("SUCCES!")) {
            assert true;
        } else assert false;


    }

    @Test
    public void stergerePrimaProgramare() {
        webDriver.manage().window().fullscreen();
        calendisLogIn.open();
        calendisLogIn.clickOnSignInClient();
        calendisLogIn.enterEmailClient("sirbsebastian97@gmail.com");
        calendisLogIn.enterPassClient(decrypt("2GWHmgZQNt/WVI/XUwTw3Q=="));
        calendisLogIn.submitLogin();
        webDriver.findElement(By.partialLinkText("PROGRAMĂRILE MELE")).click();
        int test = 0;
        try {
            if (webDriver.findElement(By.className("my-appointments-loading")).getText().equals("Nu există programări în această secțiune.")) {
                test = 1;
                assert true;
            }
        } catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println("NoSuchElementException!!");

        }


        if (test == 0) {


            int lungimeInitiala = webDriver.findElements(By.className("appointment-content")).size();

            webDriver.findElement(By.xpath("//body/section[@class='user-appointments-list']/div[@class='section-wrapper']/div[3]/div[3]/div[1]/div[1]/div[@class='appointment-card-element collapsable-item future']/div[@class='appointment-content']/h1/span[@class='collapse-caret']")).click();
            webDriver.findElement(By.xpath("//body/section[@class='user-appointments-list']/div[@class='section-wrapper']/div[3]/div[3]/div[1]/div[1]/div/div[@class='appointment-content']/div[@class='collapsable-item-content']//button[.='ȘTERGE']")).click();
            webDriver.findElement(By.xpath("//body/div[@class='warning-modal']/div[@class='warning-modal-element']//button[2]")).click();
            webDriver.navigate().refresh();
            int lungimeFinala = webDriver.findElements(By.className("appointment-content")).size();
            if (lungimeFinala + 1 == lungimeInitiala || (lungimeInitiala == 1 && lungimeFinala == 1)) {
                assert true;
            } else assert false;

        }
    }


    @Test
    public void logOut() {
        webDriver.manage().window().fullscreen();
        calendisLogIn.open();
        calendisLogIn.clickOnSignInClient();
        calendisLogIn.enterEmailClient("sirbsebastian97@gmail.com");
        calendisLogIn.enterPassClient(decrypt("2GWHmgZQNt/WVI/XUwTw3Q=="));
        calendisLogIn.submitLogin();
        webDriver.findElement(By.xpath("/html//button[@id='user-account']")).click();
        webDriver.findElement(By.xpath(" //a[@id='logOutLink']/div[1]")).click();
        webDriver.navigate().refresh();
        if (webDriver.findElement(By.xpath("/html//button[@id='login-btn']")).isDisplayed()) {
            assert true;
        } else assert false;


    }


}
