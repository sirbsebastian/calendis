package lab5.steps.serenity;

import lab5.pages.CalendisHomeClient;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class SearchStep extends ScenarioSteps {
    CalendisHomeClient calendisHomeClient;

    @Step
    public void search(String cautat)  {
        calendisHomeClient.enterSearchElement(cautat);

    }
    @Step
    public void choosedOption()  {
        calendisHomeClient.chooseOption();
    }
    @Step
    public void submitSerach(){
        calendisHomeClient.clickSearch();
    }

    @Step
    public String getSearchResult(){
        return calendisHomeClient.serachResultIs();
    }
}
