package lab5.steps.serenity;

import lab5.pages.CalendisHomeClient;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class RecomandaStep extends ScenarioSteps {
    CalendisHomeClient calendisHomeClient;

    @Step
    public void clickRecom(){
        calendisHomeClient.clickLinkRecom();
    }

    @Step
    public void writeNume(String nume){
        calendisHomeClient.inputNume(nume);
    }

    @Step
    public void submitRecomanda(){
        calendisHomeClient.submitRecomanda();
    }

    @Step
    public void writeLocation(String locatie){
        calendisHomeClient.inputLocation(locatie);
    }

    @Step
    public void writeLastName(String lastName){
        calendisHomeClient.inputLastName(lastName);
    }

    @Step
    public void writeFirstName(String firstName){
        calendisHomeClient.inputFirstName(firstName);
    }

    @Step
    public void writeEmail(String email){
        calendisHomeClient.inputEmail(email);
    }
    @Step
    public void writePhone(String phone){
        calendisHomeClient.inputPhone(phone);
    }

}
