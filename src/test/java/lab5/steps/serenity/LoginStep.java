package lab5.steps.serenity;

import lab5.pages.CalendisHomeClient;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class LoginStep extends ScenarioSteps {
    CalendisHomeClient calendisHomeClient;

    @Step
    public void open() {
        calendisHomeClient.open();
    }



    @Step
    public void clickOnSignIn() {
        calendisHomeClient.clickLogIn();
    }


    @Step
    public void clickOnSignInClient() {
        calendisHomeClient.clickLogIn();
    }

    @Step
    public void enterEmailClient(String email){
        calendisHomeClient.enterEmailClient(email);
    }


    @Step
    public void enterPassClient(String pass){
        calendisHomeClient.enterPasswordClient(pass);
    }

    @Step
    public void submit(){
        calendisHomeClient.submitLogin();
    }

    @Step
    public void submitLogin(){
        calendisHomeClient.submitLoginClient();
    }

}
